#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <vector>

// Max display size.
const int kWidth = 20;
const int kHeight = 20;
// Higher is slower.
const int kSpeed = 2;

bool game_over;
int head_x, head_y, fruit_x, fruit_y, score, frame_count;
std::vector<int> previous_x, previous_y;
enum Direction { kStop = 0, kLeft, kRight, kUp, kDown };
Direction direction, previous_direction;

// Draws all the game data to the terminal.
void Display() {
    system("cls");
    std::cout << "WASD keys to move. X to quit.\n";
    for (int i = 0; i < kWidth+2; ++i) {
        std::cout << "#";
    }
    std::cout << "\n";

    // Draw the inner mapping.
    for (int i = 0; i < kHeight; ++i) {
        std::cout << "#";

        for (int j = 0; j < kWidth; ++j) {
            bool tail_drawn = false;
            if (i == head_y && j == head_x) {
                std::cout << "O";
            } else {
                for (int k = 0; k < previous_x.size(); ++k) {
                    if (previous_x[k] == j && previous_y[k] == i) {
                        std::cout << "o";
                        tail_drawn = true;
                    }
                }
                if (i == fruit_y && j == fruit_x) {
                    std::cout << "x";
                } else if (!tail_drawn) {
                    std::cout << " ";                
                }
            }
        }
        std::cout << "#\n";
    }

    for (int i = 0; i < kWidth+2; ++i) {
        std::cout << "#";
    }
    std::cout << "\n    Score:  " << score << "\n";   
}

// Capture keyboard input for movement (WASD) and quitting (X). 
void Input() {
    if (_kbhit()) {
        previous_direction = direction;
        switch (_getch()) {
            case 'w':
                if (previous_direction != kDown) {
                    direction = kUp;
                }
                break;
            case 'a':
                if (previous_direction != kRight) {
                    direction = kLeft;
                }
                break;
            case 's':
                if (previous_direction != kUp) {
                    direction = kDown;
                }
                break;
            case 'd':
                if (previous_direction != kLeft) {
                    direction = kRight;
                }
                break;
            case 'x':
                game_over = true;
                break;
            default:
                break;
        }
    }
}

// Processes player input and changes the game data.
void Logic() {

    if (frame_count > kSpeed) {

        // Move all the tail segments forward
        for (int i = previous_x.size() - 1; i >= 0; --i) {
            if (i == 0) {
                previous_x[i] = head_x;
                previous_y[i] = head_y;
            } else {
                previous_x[i] = previous_x[i-1];
                previous_y[i] = previous_y[i-1];
            }
        }

        // Move the snake in the direction of input.
        switch (direction) {
            case kUp: 
                --head_y;
                break;
            case kDown:
                ++head_y;
                break;
            case kLeft:
                --head_x;
                break;
            case kRight:
                ++head_x;
                break;
            default:
                break;
        }

        // Wrap around to the other side when hitting a wall.
        if (head_x > kWidth - 1) {
            head_x = 0;
        } 
        if (head_x < 0) {
            head_x = kWidth - 1;
        } 
        if (head_y > kHeight - 1) {
            head_y = 0;
        } 
        if (head_y < 0) {
            head_y = kHeight - 1;
        }

        // Game over when hitting a tail segment.
        for (int i = 0; i < previous_x.size(); ++i) {
            if (head_x == previous_x[i] && head_y == previous_y[i]) {
                game_over = true;
            }
        }
        // Increase score and spawn new fruit when snake head hits a fruit.
        if (head_x == fruit_x && head_y == fruit_y) {
            ++score;
            fruit_x = rand() % kWidth;
            fruit_y = rand() % kHeight; 
            previous_x.push_back(head_x);
            previous_y.push_back(head_y);
        }

        frame_count = 0;
    }

    ++frame_count;
}

int main() {
    
    srand(time(0));
    game_over = false; 
    direction = kStop;
    head_x = kWidth / 2;
    head_y = kHeight / 2;
    fruit_x = rand() % kWidth;
    fruit_y = rand() % kHeight;
    if (head_x == fruit_x && head_y == fruit_y) {
        ++fruit_x;
    }
    score = 0;
    frame_count = 0;

    while (!game_over) {
        Display();
        Input();
        Logic();
    }

    std::cout << "------GAME OVER------\n";
    std::cout << "-press any key to quit-\n";

    _getch();

    return 0;
}